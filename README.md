# Deck Witcurve

WitCurve is the fastest growing startup in Ed-Tech domain catering services to schools and other educational institutions. Founded by graduates from different IITs and other premier institutes, team brings expertise in different technology platforms and in depth understanding of education systems practiced across Indian subcontinent. Our services include uninterrupted access to our web & mobile application with your institute’s branding. We aim to improve quality standards of education by analyzing trends from huge data sets and implementing proven data derived solutions. We believe that technology will never replace great teachers instead technology in hands of great teachers can transform a whole generation. 


### Credits

- WebSlides was created by [@sainithin] using [Cactus](https://github.com/eudicots/Cactus).
- Javascript: [@Belelros](https://twitter.com/Belelros) and [@LuisSacristan](https://twitter.com/luissacristan).
- Based on [SimpleSlides](https://github.com/jennschiffer/SimpleSlides), by [@JennSchiffer](https://twitter.com/jennschiffer).
